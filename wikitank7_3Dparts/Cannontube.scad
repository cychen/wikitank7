
Ro1 = 14 ;
Ro2 = 23 ;
Ri = 13 ;
L1 = 53.75 ;
L2 = 3 ;

module base ()
{ 
  boxx = Ro2*2 ;
  boxy = Ro2*2 ;
  boxz = L2 ;
    x = 0 ; y = Ro2+Ro1 ; z = L2/2 ;
    sx = 18.75 ; R=2 ;
  difference () {
    cylinder ( h=L2, r=Ro2);    
    for ( j = [y,-y]) {
      translate ([x,j,z]) 
        cube ([boxx,boxy,boxz],center=true);
    }
    for ( i = [sx, -sx]) {
      translate ([i,0,0]) 
      cylinder (h=L2,r=R);
    }
  }
}

module Bulletfeedtube ()
{
  color ("red") 
  difference () {
    union () {
      cylinder (h=L1,r=Ro1); 
      base () ;
    }
    cylinder (h=L1,r=Ri);
  }  
}

Bulletfeedtube () ;
