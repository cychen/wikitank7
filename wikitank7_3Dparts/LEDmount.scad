
Mx = 46 ;
My = 32 ;
Mz = 4 ;
cutx = 27 ;
cuty = 20 ;
cutz = Mz ;

module mount ()
{ 
  difference () {
    cube ([Mx,My,Mz],center=true);
    cube ([cutx,cuty,cutz],center=true);
  }
} 

module LEDhole ()
{
  x = 6.5; y = 12.5 ; z = 0 ;
  R = 1.5 ; 
     
  for ( i = [x, -x]) {
    for ( j = [y, -y]) {
      translate ([i,j,-z])
      cylinder (h=Mz+0.2,r=R,center=true);
    }
  }
}

module mhole ()
{
    x = 19.5 ; y = 0 ; z = 0 ; R = 2 ;
    for ( i = [x,-x]) {    
     translate ([i,y,z]) 
       cylinder ( h=Mz+0.2, r=R,center=true);
    }
}
    
module LEDmount ()
{    
  difference () {    
    color ("blue")
    mount (); 
    LEDhole ();  
    mhole ();      
  }
}

translate ([0,0,Mz/2])  
LEDmount ();
