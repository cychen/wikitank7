
basex = 46 ;
basey =  113 ;
basez = 4 ;

midx = 78 ;
midy = 52 ;
midz = basez ;


module base ()
{ 
    x = 0 ; y = 14.5 ; z = 0 ;
    
    union () {
      cube ([basex,basey,basez],center=true);    
      translate ([x,y,z]) 
        cube ([midx,midy,midz],center=true); 
    }    
}

module cannonhole ()
{
    x = 7 ;
    y1 = 47.5 ; y2 = 52.674 ;
    y3 = 37.5 ; y4 = 52.326 ; z = 0 ;
    R = 1.5 ;
    for ( i = [x, -x] ) {
      for ( j = [y1,y2,-y3,-y4]) {
        translate ([i,j,z])
          cylinder (h=basez,r=R,center=true);
      }
    }
}

module mhole ()
{
  x1 = 35 ; x2 = 19 ;
  y1 = 36.5 ; y2 = 7.5 ; y3 = 22.5 ;
  z = 0 ;
  R = 2 ;
  for ( i = [x1, -x1]) {
    for ( j = [y1,-y2]) {
    translate ([i,j,z]) cylinder (h=basez,r=R,center=true);
    }
  }
  translate ([x2,-y3,z]) 
    cylinder (h=basez,r=R,center=true);
}

module Cannonplateform ()
{
  color ("red") 
  difference () {
    base ();
    cannonhole ();
    mhole () ;
  }
}

translate ([0,0,basez/2])
Cannonplateform () ;
