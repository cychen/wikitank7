
basex = 46 ;
basey =  69 ;
basez = 4 ;

module base ()
{
    cube ([basex,basey,basez],center=true);     
}

module Bulletinhole ()
{
    x = 0 ; y = 14.75 ; z = 0 ; 
    R = 11.5 ;
    translate ([x,-y,z])
      cylinder (h=basez,r=R,center=true);
}

module phole ()
{
    x = 10 ;
    y1 = 31 ; y2 = 28 ; z = 0 ;
    R1 = 2; R2 = 1.5 ;
    for ( i = [x, -x] ) {
      translate ([i,-y1,z])
      cylinder (h=basez,r=R1,center=true);
    }
    for (i = [x,-x]) {
      translate ([i,y2,z]) 
        cylinder (h=basez,r=R2,center=true);
    }
}

module LEDmhole ()
{
    x = 19.5 ; y = 31 ; z = 0 ; R = 1.5 ;
    for ( i = [x,-x]) {
      translate ([i,-y,z])
        cylinder (h=basez,r=R,center=true);
    }
}

module Bulletscrewhole ()
{
  x = 16.75 ; 
  y = 14.75 ;
  z = 0 ;
  R = 1.5 ;
  for ( i = [x, -x]) {
    translate ([i,-y,z]) cylinder (h=basez,r=R,center=true);
  }
}

module Motorscrewhole ()
{
  x = 19 ; 
  y1 = 5 ; y2 = 20.5 ;
  z = 0 ;
  R = 1.5 ;
  for ( i = [x, -x]) {
    for ( j = [y1, y2]) {
      translate ([i,j,z]) cylinder (h=basez,r=R,center=true);
    }
  }
}

module Cannontopcover ()
{
  color ("red") 
  difference () {
    base ();
    Bulletinhole () ;
    phole ();
    Bulletscrewhole () ;
    Motorscrewhole () ;
    LEDmhole ();
  }
}

translate ([0,0,basez/2])
Cannontopcover () ;
