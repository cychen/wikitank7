
boxx=23;
boxy=8;
boxz=31.899;
mboxx=boxx; mboxy=boxy; mboxz=4;

module box ()
{
 cube ([boxx,boxy,boxz],center=true);
} 

module taphole ()
{
   x = 7 ; y = 1.294 ; z=10.048; R=1.5;
   for ( i = [x,-x]) {
     translate ([i,y,z]) rotate ([15,0,0])
       cylinder (h= 10+1 ,r=R,center=true);
   }
}

module mbox ()
{
    mx = 0 ; my = 8 ; mz = 13.95 ;
    hx = 7 ; hy = 0 ; hz = 0 ; R = 2 ;
  translate ([mx,my,-mz]) {    
    difference () {
      cube ([mboxx,mboxy,mboxz],center=true);  
      for ( i = [hx, -hx]) {
      translate ([i,hy,hz]) 
        cylinder (h=mboxz,r=R,center=true);
      }
    }
  }
}

module cutbox ()
{
    x = 0 ; y = 0 ; z = 14.878 ;
    translate ([x,y,z])
    rotate ([15,0,0])
    translate ([x,y,boxz/2]) 
     cube ([boxx,boxy+2,boxz],center=true);
}

module BulletbasenotiltB ()
{    
  union () {
    difference () {     
      box ();
      cutbox ();  
      taphole ();      
    }
    mbox ();
  }
}

translate ([0,0,boxz/2])  
BulletbasenotiltB();
