
Headx = 46 ; Heady = 20 ; Headz = 8 ;
cutx = 11 ;cutyt = 5 ; cutyb = 3 ;
cutzt = 4 ; cutzb = 2 ;
Neckx= 12 ; Necky = 20 ; Neckz = Headz;
Basex = 32 ; Basey = 3 ; Basez = 16 ;

module sensorhole ()
{
  x = 13; y = 0 ; z = 0 ; R = 8.5 ; 
     
  for ( i = [x, -x]) {
    translate ([i,y,z])
      cylinder (h=Headz+0.2,r=R,center=true);
  }
}

module topcut ()
{
  x = 0 ; y = 7.5 ; z = 4 ;
  translate ([x,y,z])
    cube ([cutx,cutyt,Headz],center=true);
}

module bottomcut ()
{
  x = 0 ; y = 8.5 ; z = 6 ;
  translate ([x,-y,z])
    cube ([cutx,cutyb,Headz],center=true);
}
    
module Head ()
{    
  difference () {    
    color ("blue")
    cube ([Headx,Heady,Headz],center=true); 
    sensorhole (); 
    topcut () ;
    bottomcut () ;      
  }
}

module Neck ()
{
    x = 0 ; y = 20; z=0 ;
    translate ([x,-y,z]) 
      cube ([Neckx, Necky, Neckz],center=true);
}

module Base ()
{
    x = 0; y=31.5 ; z=4;
    hx = 10.7 ; hy =0;  hz = 3; Rs = 1.2 ; Rb = 3 ;
    
  translate ([x,-y,z]) {
    difference () {
      cube ([Basex, Basey, Basez],center=true);
      for ( i = [hx,-hx]) {
        translate ([i,hy, hz]) rotate ([90,0,0])
          cylinder ( h=Basey+0.2,r=Rs,center=true);
      }
      translate ([0,0,hz]) rotate ([90,0,0])
        cylinder (h=Basey+0.2, r=Rb, center=true);
      }
    }
}

module Ultrasonicsensorpanel ()
{    
  union () {    
    Head (); 
    Neck (); 
    Base ();      
  }
}

translate ([0,15,Headz/2]) 
Ultrasonicsensorpanel ();
