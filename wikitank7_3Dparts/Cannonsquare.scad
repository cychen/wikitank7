
boardx=29 ;
boardy=39 ;
boardz=6 ;
cutx = 12 ;
wallx = 3 ;
wally = 11 ;
wallz = 11 ;

module wall ()
{
    x = 13 ; y = 14  ; z = 8.5 ;
    for ( i = [x,-x]) {
      translate ([i,y,z]) cube ([wallx,wally,wallz],center=true);
    }
}

module Cannonsquare ()
{
    y1 = 6 ; y2 = 15 ;
    R = 1.5;
  union () {
    difference () {    
      color ("blue")  
      cube ([boardx,boardy,boardz],center=true);
      translate ([0,0,4]) 
        cube ([cutx,boardy,boardz],center=true);
      for ( j = [y1, -y2]) {
        translate ([0,j,0]) 
          cylinder (h=boardz,r=R,center=true);
      }
    }
    wall ();
  }
}

translate ([0,0,boardz/2])  
Cannonsquare();
