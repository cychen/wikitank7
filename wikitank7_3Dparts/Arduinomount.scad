
boardx=43;
boardy=6;
boardz=6;
cutx = 9 ;
cuty = boardy ;
cutz = boardz ;

module mhole ()
{
  x1 = 12.5 ;  x2 = 17.5;  
  y = 0 ; z = 0;
  R = 1.75 ;
    
    for (i = [x1,-x2]) {
      translate ([i,y,z])
        cylinder (h=boardz,r=R,center=true);
    }
} 

module Arduinohole ()
{
  x1 = 18.5; x2 = 10 ;
  y = 0 ; z = 0 ;
  R = 1.5 ; 
     
  for ( i = [x1, -x2]) {
    translate ([i,y,z]) rotate ([90,0,0])
      cylinder (h=boardz+0.2,r=R,center=true);
  }
}

module cut ()
{
    x = 2.5 ; y = 3;
    z = 0 ;
        
    translate ([-x,y,z]) 
    cube ([cutx,cuty,cutz],center=true);
}

module Arduinomount ()
{    
  difference () {    
    color ("blue")  
    cube ([boardx,boardy,boardz],center=true);
    mhole ();  
    Arduinohole (); 
//    cut ();     
  }
}

translate ([0,0,boardz/2])  
Arduinomount ();
