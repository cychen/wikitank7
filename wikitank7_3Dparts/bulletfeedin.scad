
basex = 31 ;
basey =  30 ;
basez = 6 ;
sidez = 26 ;
cutx = 12 ;
sidex = 4 ;
backy = 7 ;

module base ()
{
  difference () {  
    cube ([basex,basey,basez],center=true);
    translate ([0,0,4]) cube ([cutx,basey,basez],center=true);
  }        
}

module sides ()
{
    x = 13.5 ; y = 0 ; z = 10 ;
    for ( i = [x,-x]) {
      translate ([i,y,z]) cube ([sidex,basey,sidez],center=true);
    }
}

module back ()
{
    x = 0 ; y = 11.5 ; z = 10 ;
    translate ([0,-y,z]) cube ([basex,backy,sidez],center = true);
}

module ScotchYorkhole ()
{
    x = 0 ; y = 11.5 ; z = 11.7 ; 
    R = 3.5 ;
    translate ([x,-y,z]) rotate ([90,0,0])
      cylinder (h=backy,r=R,center=true);
}

module phole ()
{
    x = 10 ; y = 11.5 ; z= 19.5 ;
    R = 1.5;
    for (i = [x,-x]) {
      translate ([i,-y,z]) 
        cylinder (h=7,r=R,center=true);
    }
}

module basescrewhole ()
{
  x = 0 ; 
  y1 = 10.25 ; y2 = 10.75 ;
  z = 0 ;
  R = 1.5 ;
  for ( j = [y1, -y2]) {
    translate ([x,j,z]) cylinder (h=basez,r=R,center=true);
  }
}

module motorscrewhole ()
{
  x = 13.5 ; 
  y1 = 1.25 ; y2 = 4.75 ; y3 = 2.25 ;
  z1 = 14.95 ; z2 = 6.45 ;
  R = 1.5 ;
    for ( i = [x,-x]) {
        translate ( [i,-y1,z1]) rotate ([0,90,0]) 
          cylinder (h=sidex,r=R,center=true) ;
        for ( j = [-y2, y3]) {
          translate ([i,j,z2]) rotate ([0,90,0])
            cylinder (h=sidex,r=R,center=true);
        }
    }
}

module bulletfeedin ()
{
  color ("red") 
  difference () {
    union () {
      base ();
      sides ();
      back ();
    }
    ScotchYorkhole () ;
    phole ();
    basescrewhole () ;
    motorscrewhole () ;
  }
}

translate ([0,0,basez/2])
bulletfeedin () ;
