
Mx = 61.3 ;
My = 16 ;
Mz = 3 ;

module mount ()
{ 
    cube ([Mx,My,Mz],center=true);
} 

module mhole ()
{
    x = 26.65 ; y = 2 ; z = 0 ; R = 1.5 ;
    for ( i = [x,-x]) {    
     translate ([i,-y,z])
       cylinder ( h=Mz+0.2, r=R,center=true);
    }
}
    
module UltrasonicsensorpanelB ()
{    
  difference () {    
    color ("blue")
    mount ();  
    mhole ();      
  }
}

 translate ([0,0,Mz/2])
UltrasonicsensorpanelB ();
