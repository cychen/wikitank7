
Mx = 59 ;
My = 40.4;
Mz = 3 ;

module plate ()
{
  cube ([Mx,My,Mz],center=true);
} 

module hole ()
{
  x1 = 26; x2 = 26.5 ; y1 = 17.2 ; y2 = 16.7;
  z = 0 ; R = 2 ; 
     
  for ( i = [[x1, y1,z],[-x2,-y2,z]]) {
    translate (i) 
      cylinder (h=Mz+0.2,r=R,center=true);
  }
}

module cut1 ()
{
  x = 7 ; y = 19.5 ; z = 0 ;  
  translate ([-x,y,z]) plate ();
}

module cut2 ()
{
  x = 45 ; y = 7 ; z = 0 ;  
  translate ([-x,y,z]) plate ();
}

module cut3 ()
{
  x = 20 ; y = 30.9 ; z = 0 ;  
  translate ([x,-y,z]) plate ();
} 

module WIFIplate ()
{    
  difference () {    
    plate ();
    cut1 () ;
    cut2 ();
    cut3 ();  
    hole ();      
  }
}

translate ([0,0,Mz/2])
WIFIplate ();
