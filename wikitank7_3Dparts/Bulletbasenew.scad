
boardx=23;
boardy=91;
boardz=4;

module cannonhole ()
{
  x = 7 ;    
  y1 = 8.5 ; y2 = 21.75;
  Rs = 2 ; 
    
    for ( i = [x,-x]) {
      for (j = [y1, -y2]) {
        translate ([i,j,0]) {
          cylinder (h=boardz,r=Rs,center=true); 
        }
      }
    }
} 

module mhole ()
{
  x = 7;
  y = 41.5 ;
  Rs = 2 ;
     
  for ( i = [x, -x]) {
    for ( j = [y,-y]) {
      translate ([i,j,0]) {
        cylinder (h=boardz,r=Rs,center=true);
      }
    }
  }
}

module Bulletbasenew ()
{    
  difference () {    
    color ("blue")  
    cube ([boardx,boardy,boardz],center=true);
    cannonhole ();  
    mhole ();      
  }
}

translate ([0,0,boardz/2])  
Bulletbasenew ();
