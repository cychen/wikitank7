
Mx = 33.5 ;
My = 8 ;
Mz = 16.5 ;
cutx = 23.5 ;
cuty = 8 ;
cutz = 16.5 ;

module mount ()
{
  x= 0; y=0 ; z = 4 ;
  
  difference () {
    cube ([Mx,My,Mz],center=true);
    translate ([x,y,-z])
      cube ([cutx,cuty,cutz],center=true);
    }
} 

module FS90Rhole ()
{
  x = 14.25;
  y = 0 ; z = 2 ;
  R = 1.2 ; 
     
  for ( i = [x, -x]) {
    translate ([i,y,-z]) rotate ([90,0,0])
      cylinder (h=My+0.2,r=R,center=true);
  }
}

module base ()
{
    basex = 8 ;
    basey = My ;
    basez = 4 ;
    x = 20.75 ; y = 0;
    z = 6.25 ;
    
   for ( i = [x,-x]) {    
     translate ([i,y,-z]) 
       cube ([basex,basey,basez],center=true);
   }
}

module mhole ()
{
    x = 20.75 ; y = 0 ; z = 6.25 ; R = 2 ;
    for ( i = [x,-x]) {    
     translate ([i,y,-z]) 
       cylinder ( h=4, r=R,center=true);
    }
}
    
module FS90Rmount ()
{    
  difference () {    
    color ("blue")
    union () {
      mount ();
      base () ;
    }  
    FS90Rhole ();  
    mhole ();      
  }
}

translate ([0,0,My/2]) rotate ([90,0,0]) 
FS90Rmount ();
