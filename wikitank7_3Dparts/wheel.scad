
wheelOR = 38 ;
wheelORt = 7 ;
wheelt = 10 ;
wheelIR = 12 ;
horn = 8.4 ;

module rimslot ()
{
   translate ([wheelOR,0,0]) 
    cube ([5,3,wheelt+1],center=true);
}

module centerslot ()
{
    difference () {
      cylinder (r=wheelOR, h=5.5,center=true);
      cylinder (r=wheelOR-3,h=5.5,center=true);
    }
}

module outer ()
{
  difference () {
    cylinder (r=wheelOR,h=wheelt,center=true);
    cylinder (r=(wheelOR-wheelORt),h=wheelt+0.2,center=true);
    centerslot ();
 //   for (i=[0:35]) {
 //     rotate (i*10, [0,0,1]) 
 //       rimslot ();
 //   }
  }
}

module bone ()
{
  translate ([(wheelIR),0,0]) { 
    hull () {
      cube ([2,5,wheelt],center=true);
      translate ([(wheelOR-wheelIR-wheelORt),0,0]) 
      cube ([2,9,wheelt],center=true);
    }
  }
}

module axial ()
{
  for (i= [0:7]) {
    rotate (i*45,[0,0,1]) bone ();
  }
  cylinder (r=wheelIR,h=wheelt,center=true);
}

module hole ()
{
    x = horn ; y = 0 ; z = 0; Rs=1.4 ; Rb = 3 ;
  for (i =[x,-x] ) { 
      translate ([i,y,z]) 
        cylinder (r=Rs,h=wheelt,center=true);
  }
  translate ([0,0,0]) 
      cylinder (r=Rb,h=wheelt,center=true);
}

module cut ()
{
  x = 0 ; y=0; z = 2;
  translate ([x,y,z]) cylinder (r=(wheelOR-wheelORt),h=6, center=true);
}

module wheel ()
{  
  difference () {
    union () {
      outer ();
      axial ();         
    }
    hole ();
    cut ();
  }
}

translate ([0,0,wheelt/2]) 
wheel ();

