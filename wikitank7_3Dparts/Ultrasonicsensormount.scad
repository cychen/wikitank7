
Mx = 60 ;
My = 17 ;
Mz = 4 ;

module mount ()
{ 
    cube ([Mx,My,Mz],center=true);
} 

module SG90hole ()
{
  x = 10.7;
  y = 2.5 ; z = 0 ;
  Rs = 1.2 ; Rc = 3 ;
     
  for ( i = [x, -x]) {
    translate ([i,y,z])
      cylinder (h=Mz+0.2,r=Rs,center=true);
  }
  translate ([0,y,z]) 
    cylinder (h=Mz+0.2,r=Rc,center=true);
}

module mhole ()
{
    x = 26 ; y = 4.5 ; z = 0 ; R = 2 ;
    for ( i = [x,-x]) {    
     translate ([i,-y,z]) 
       cylinder ( h=Mz+0.2, r=R,center=true);
    }
}
    
module Ultrasonicsensormount ()
{    
  difference () {    
    color ("blue")
    mount (); 
    SG90hole ();  
    mhole ();      
  }
}

translate ([0,0,Mz/2])  
Ultrasonicsensormount ();
