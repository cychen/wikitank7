
Mx = 61.3 ;
My = 25 ;
Mz = 8 ;
cutx = 11 ;
cutyt = 5 ; cutyb = 3 ;
cutzt = 4 ; cutzb = 2 ;

module mount ()
{ 
    cube ([Mx,My,Mz],center=true);
} 

module sensorhole ()
{
  x = 13;
  y = 2.5 ; z = 0 ;
  R = 8.5 ; 
     
  for ( i = [x, -x]) {
    translate ([i,y,z])
      cylinder (h=Mz+0.2,r=R,center=true);
  }
}

module top ()
{
  x = 0 ; y = 10 ; z = 4 ;
  translate ([x,y,-z])
    cube ([cutx,cutyt,Mz],center=true);
}

module bottom ()
{
  x = 0 ; y = 6 ; z = Mz-cutzb ;
  translate ([x,-y,-z])
    cube ([cutx,cutyb,Mz],center=true);
}

module mhole ()
{
    x = 26.65 ; y = 2.5 ; z = 4 ; Rs = 2 ;
    Rb = 3 ; Rv = 1.5 ; yv = 6.5 ; 
    for ( i = [x,-x]) {    
     translate ([i,y,z])
       cylinder ( h=Mz+0.2, r=Rb,center=true);
     translate ([i,y,0])
       cylinder ( h=Mz+0.2, r=Rs,center=true);
     translate ([i,-yv,0]) rotate ([90,0,0])
       cylinder (h=16,r=Rv,center=true);
    }
}
    
module UltrasonicsensorpanelA ()
{    
  difference () {    
    color ("blue")
    mount (); 
    sensorhole (); 
    top () ;
    bottom () ; 
    mhole ();      
  }
}

 translate ([0,0,Mz/2])  rotate ([0,180,0])
UltrasonicsensorpanelA ();
