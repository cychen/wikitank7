
Ro1 = 6 ;
Ro2 = 2.5 ;
Ri = 1.5 ;
L1 = 3 ;
L2 = 4 ;

module ScotchYorktip ()
{
    difference () {
      union () {    
      cylinder (h=L1,r=Ro1);
      translate ([0,0,L1]) 
        cylinder (h=L2,r=Ro2);
      } 
      cylinder (h=(L1+L2),r=Ri);
    }
}

ScotchYorktip () ;
