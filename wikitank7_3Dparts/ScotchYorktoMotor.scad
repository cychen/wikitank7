vox = 24 ;
voR = 5 ;
voz = 4 ;
x1 = 12 ;
x2 = 6.5 ;

module ScotchYorktoMotor ()
{
  difference () {
    translate ([-vox/2,0,0]){ 
      hull () {
        cylinder (h=voz,r=voR);
        translate ([vox,0,0]) 
          cylinder (h=voz,r=voR);
      }
    }     
    for (i= [x1,-x1]) {
      translate ([i,0,0]) 
        cylinder (h=voz,r=1.5);
    } 
    for (i= [x2,-x2]) {
      translate ([i,0,0]) 
        cylinder (h=voz,r=1.2);
    }    
    cylinder (h=voz,r=2.25);
  }
}

ScotchYorktoMotor () ;
