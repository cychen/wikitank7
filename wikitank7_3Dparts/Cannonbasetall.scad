
boardx=23;
boardy=69;
boardz=20;

module chole ()
{
  cx = 0 ;    
  cy1 = 0 ; cy2 = 21;
  cy3 = 9.25 ; cy4 = 30.25 ;
  Rs = 2 ; Rb = 4 ;
    
    for (j = [cy1, cy2, -cy3, - cy4]) {
      translate ([cx,j,0]) {
        union () {
          cylinder (h=boardz,r=Rs,center=true);
          translate ([0,0,-4]) cylinder (h=boardz,r=Rb,center=true);
        }
      }
    }
} 

module nchole ()
{
  ncx = 7;
  ncy1 = 10.5 ; ncy2 = 19.75;
  Rs = 1.5 ; Rb = 4 ;
     
  for ( i = [ncx, -ncx]) {
    for ( j = [ncy1,-ncy2]) {
      translate ([i,j,0]) {
        union () {
          cylinder (h=boardz,r=Rs,center=true);
          translate ([0,0,4]) cylinder (h=boardz,r=Rb,center=true);
        }
      }
    }
  }
}

module motorhole ()
{
    y1 = 28.5 ; y2 = 7.5;
    z = 5.5 ;
    R = 1.5 ;
    for ( j = [y1, -y2]) {
      for ( k = [z, -z]) {
        translate ([0,j,k]) rotate ([0,90,0]) cylinder (h=boardx,r=R,center=true);
      }
    }
}

module phole ()
{
    x = 7.5 ;
    y = 31 ; z = 0;
    R = 1.5 ;
    for ( i = [x, -x]) {
      translate ([i,y,z]) rotate ([90,0,0]) cylinder (h=7,r=R,center=true);
    }
}

module Cannonbase ()
{    
  difference () {    
    color ("blue")  
    cube ([boardx,boardy,boardz],center=true);
    chole ();  
    nchole (); 
    motorhole ();
    phole ();      
  }
}

translate ([0,0,boardz/2])  
Cannonbase();
