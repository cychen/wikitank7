boxx = 44 ;
boxy = 20 ;
boxz = 4 ;

module motorhole ()
{
    x = 10.75 ;
    y1 = 1.5 ; y2 = 6.5 ;
    R = 1.2 ;
    
    for ( i = [x, -x]) {
      for (j = [y1, y2]) {
        translate ([i,j,0]) cylinder (h=boxz+0.2,r=R, center=true);
      }
    }
}

module mhole ()
{
    x = 18 ;
    y = 5.5 ; 
    R = 2.0 ;
    
    for ( i = [x, -x]) {
      for ( j = [y, -y]) {
        translate ([i,j,0]) 
          cylinder (h=boxz+0.2,r=R, center=true);
      }
    }
}

module motorangle ()
{ 
  difference () {
    cube ([boxx, boxy, boxz],center=true);
    motorhole ();
    mhole ();
  }
}


translate ([0,0,boxz/2]) 
motorangle () ;
