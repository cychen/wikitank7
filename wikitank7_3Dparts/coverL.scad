
basex = 28 ;
basey = 10 ;
basez = 3 ;

module bbase ()
{ 
    x = 10 ; y = 1.5 ; z = 0 ; R = 2 ;
    difference () {
      cube ([basex,basey,basez],  center=true);
      for ( i = [x , -x]) {
        translate ([i,-y,z]) 
          cylinder (h=basez,r=R,center=true) ;
      }
    }
}

module vbase ()
{ 
    x = 0 ; y = 3.5 ; z = 3.5 ; R = 1.5 ;
    translate ([x,y,z]) {
      difference () {
        cube ([basex,basez,basey],  center=true);
        translate ([0,0,1.5]) 
          rotate ([90,0,0])
            cylinder (h=basez+0.2,r=R,center=true) ;
      }
    }
}


module coverL ()
{
  color ("red") 
  union () {
    bbase ();
    vbase ();
  }
}

translate ([0,0,basez/2])
coverL () ;
