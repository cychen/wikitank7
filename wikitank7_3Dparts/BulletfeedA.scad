Ro = 12.75 ;
Ri = 11.25 ;
cL = 37.5 ;
boxx = 2*Ro ;
boxy = Ri ;
boxz = cL ;
earx = 9.5 ;
eary = 3 ;
earz = cL ;
basex = earx ;
basey = 17.25 ;
basez = 3 ;

module halfcyl ()
{
  difference () {      
    cylinder (h=cL, r=Ro,center=true);
    cylinder (h=cL,r=Ri,center=true);
    translate ([0,Ro,0]) cube ([2*Ro+2,2*Ro,cL],center=true); 
  }
}

module flatpart ()
{ 
  translate ([0,boxy/2,0]) {
    difference () {
      cube ([boxx, boxy, boxz],center=true);
      cube ([boxx-2*(Ro-Ri),boxy,boxz],center=true);
    } 
  }    
}

module ear ()
{
  union () {
    difference () {
      cube ([earx, eary, earz], center=true);
      translate ([0.75,0,4]) rotate ([90,0,0])    cylinder (h=eary+0.2,r=1.5,center=true);
      } 
    }  
    translate ([0,-7.125,-17.25]) {
      difference () {    
        cube ([basex,basey,basez], center=true);
        translate ([0.75,-2.625,0]) 
          cylinder (h=basez+0.2,r=2,center=true);
    }
  }  
}

module BulletfeedA ()
{
  x = 16 ; y = 9.75 ; z = 0 ;
  union () {
    halfcyl ();
    flatpart ();
      translate ([x,y,z]) ear ();
      translate ([-x,y,z]) mirror ([1,0,0]) ear ();
    } 
}

translate ([0,0,cL/2]) 
BulletfeedA () ;
