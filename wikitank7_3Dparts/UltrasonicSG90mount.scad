
Mx = 55 ;
My = 18.5 ;
Mz = 4 ;
cutx = 23.5 ;
cuty = 12.5 ;
cutz = Mz ;

module mount ()
{ 
  difference () {
    cube ([Mx,My,Mz],center=true);
    cube ([cutx,cuty,cutz],center=true);
  }
} 

module SG90hole ()
{
  x = 14.25;
  y = 0 ; z = 0 ;
  R = 1.2 ; 
     
  for ( i = [x, -x]) {
    translate ([i,y,-z])
      cylinder (h=Mz+0.2,r=R,center=true);
  }
}

module mhole ()
{
    x = 21.5 ; y = 1.75 ; z = 0 ; R = 2 ;
    for ( i = [x,-x]) {    
     translate ([i,-y,-z]) 
       cylinder ( h=4, r=R,center=true);
    }
}
    
module UltrasonicSG90mount ()
{    
  difference () {    
    color ("blue")
    mount (); 
    SG90hole ();  
    mhole ();      
  }
}

translate ([0,0,Mz/2])  
UltrasonicSG90mount ();
