
px = 54.5 ;
py = 4 ;
pz = 16.5 ;
motorx = 23.5 ;
basex = 37.5 ;
basey = 10 ;
basez = 4 ;

module frontp ()
{ 
    x = 8.5 ; y =0 ; z = 4;
  difference () {
    cube ([px,py,pz],center=true);
    translate ([-x,y,z]) cube ([motorx,py,pz],center=true) ;
  }
}

module base ()
{
  x = 8.5 ; y = 3 ; z = 6.25 ;
  translate ([-x,y,-z]) 
    cube ([basex,basey,basez],center=true);
}  

module screwhole (R)
{
    rotate ([90,0,0])
      cylinder (h=py+0.2,r=R,center=true);
}

module mhole ()
{
    x1 = 22.75 ; x2 = 5.75 ; 
    x3 = 19.75 ; x4 = 16.25 ; x5 = 23.25 ; 
    y = 0 ; z1 = 2 ; z2 = 4.25 ;
    
    for ( i = [-x1, x2]) {
      translate ([i,y,z1]) screwhole (R=1.2);
    }
    translate ([x3,y,z2]) screwhole (R=2);
    for ( i = [x4,x5]) {
      translate ([i,y,-z2]) screwhole (R=2);
    }
}

module MineSG90fpnew ()
{
  color ("red") 
  difference () {
    union () {
      frontp (); 
      base () ;
    }
    mhole ();
  }  
}

translate ([0,0,py/2]) rotate ([90,0,0])
MineSG90fpnew () ;
