
Ro1 = 12.75 ;
Ro2 = 20.75 ;
Ri = 11 ;
L1 = 53.75 ;
L2 = 3 ;

module base ()
{ 
  boxx = Ro2*2 ; boxy = Ro2*2 ; boxz = L2 ;
    x = 0 ; y = Ro2+Ro1 ; z = 0 ;
    sx = 16.75 ; R=1.75 ;
  difference () {
    cylinder ( h=L2, r=Ro2,center=true);    
    for ( j = [y,-y]) {
      translate ([x,j,z]) 
        cube ([boxx,boxy,boxz],center=true);
    }
    for ( i = [sx, -sx]) {
      translate ([i,0,0]) 
      cylinder (h=L2,r=R,center=true);
    }
  }
}

module Bulletfeedtube ()
{
    x = 0 ; y = 0 ; z = -25.375 ;  
  difference () {
    union () {
      cylinder (h=L1,r=Ro1,center=true); 
      translate ([x,y,z]) base () ;
    }  
    cylinder (h=L1,r=Ri,center=true);
  }   
}

translate ([0,0,L1/2])
Bulletfeedtube () ;
