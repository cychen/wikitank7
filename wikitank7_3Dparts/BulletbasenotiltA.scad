
boxx=23;
boxy=8;
boxz=10.417;
mboxx=8; mboxy=8; mboxz=4;

module box ()
{
 cube ([boxx,boxy,boxz],center=true);
} 

module taphole ()
{
   x = 7 ; y = 1.292 ; z=0; R=1.5;
   for ( i = [x,-x]) {
     translate ([i,y,z]) rotate ([15,0,0])
       cylinder (h=boxz+1,r=R,center=true);
   }
}

module mbox ()
{  
    x = 0 ; y = 8 ; z = 3.209 ;
    Rs = 2 ; 
  translate ([x,-y,-z]) {
    difference (){
      cube ([mboxx,mboxy,mboxz],center=true);
      cylinder (h=mboxz,r=Rs,center=true);
    }
  }
}

module cutbox ()
{
    x = 0 ; y = 0 ; z = 4.137 ;
    translate ([x,y,z])
    rotate ([15,0,0])
    translate ([x,y,boxz/2]) 
     cube ([boxx,boxy+2,boxz],center=true);
}

module BulletbasenotiltA ()
{    
  union () {
    difference () {      
    box ();
    cutbox ();  
    taphole ();     
    }
    mbox ();
  }
}

translate ([0,0,boxz/2])  
BulletbasenotiltA();
