
basex = 46 ;
basey =  60 ;
basez = 4 ;

module base ()
{
    cube ([basex,basey,basez],center=true);     
}

module Bulletexithole ()
{
    x = 0 ; y = 4.7 ; z = 0 ; 
    R = 12.5 ;
    translate ([x,y,z])
      cylinder (h=basez,r=R,center=true);
}

module phole ()
{
    x1 = 0 ; x2 = 7.5 ;
    y1 = 26.5 ; y2 = 20 ; z = 0 ;
    R = 2;
    translate ([x1,y1,z])
      cylinder (h=basez,r=R,center=true);
    for (i = [x2,-x2]) {
      translate ([i,-y2,z]) 
        cylinder (h=basez,r=R,center=true);
    }
}

module Bulletscrewhole ()
{
  x = 18.75 ; 
  y = 4.7 ;
  z = 0 ;
  R = 1.5 ;
  for ( i = [x, -x]) {
    translate ([i,y,z]) cylinder (h=basez,r=R,center=true);
  }
}

module Cannonfrontcover ()
{
  color ("red") 
  difference () {
    base ();
    Bulletexithole () ;
    phole ();
    Bulletscrewhole () ;
  }
}

translate ([0,0,basez/2])
Cannonfrontcover () ;
