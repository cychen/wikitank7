vox = 24 ;
voR = 5 ;
voz = 4 ;
vix = vox ;
viR = 3 ;
viz = voz ;
hx = 4;
hy = 33 ;
hz = voz ;

module vpart ()
{
  difference () {
    hull () {
      cylinder (h=voz,r=voR);
      translate ([vox,0,0]) 
        cylinder (h=voz,r=voR);
    }  
    hull () {   
      cylinder (h=voz,r=viR);    
      translate ([vox,0,0]) 
        cylinder (h=voz,r=viR);
    }
  }
}


module hpart ()
{
    cube ([hx,hy,hz],center=true );
}

module ScotchYork ()
{
  color ("red") 
  union () {
    translate ([-vox/2,0,-voz/2]) vpart ();
    translate ([0,hy/2+voR,0]) hpart ();
  }
}

translate ([0,-15,voz/2]) 

ScotchYork () ;
