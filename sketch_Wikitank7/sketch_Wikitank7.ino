
//範例會用到Servo函式庫提供的函式,因此先將此函式庫載入.

#include <Servo.h>
#include <NewPing.h>

Servo Sonar ;
Servo LW ;
Servo RW ;
Servo ScotchYork ; 

#define MAX_DISTANCE 50 
#define TRIGGER_PIN 8  
#define ECHO_PIN  9 
NewPing sonar(8,9,100);

#define Sonar_PIN 10
#define LW_PIN 4 
#define RW_PIN 5
#define ScotchYork_PIN 7

#define Cannon_LM_Red_PIN  3  //PWMA
//# define Cannon_LM_Black_PIN 4 
//# define Cannon_RM_Black_PIN 7
#define Cannon_RM_Red_PIN  6  //PWMB             

#define Interrrupt1_PIN 2 
#define HC595_Latch_PIN 11 //ST_CP pin
#define HC595_Data_PIN 12  //DS pin
#define HC595_Clock_PIN 13 //SH_CP pin


int PIN_Array [10] = {3,4,5,6,7,8,10,11,12,13};

#define angle_mid  90   
#define angle_right  60  
#define angle_left  120 
const int angle_number = 3 ;
int angle_increment = (angle_left - angle_right ) / (angle_number - 1) ;
int angleArray [angle_number];
int distanceArray [angle_number];

#define LWStop 1500 
#define RWStop 1500 
#define Cannon_hold 10 //angle
#define Cannon_push 10 
#define Cannon_load 140

volatile int Hit_Flag = LOW;
int Hit_Count = 0;
int Life_Remained ;
int Seg_Array [10] = {192,249,164,176,153,146,130,248,128,152};//7-seg LED value

void setup ()
{
   LW.attach (LW_PIN);
   RW.attach (RW_PIN);
   Sonar.attach (Sonar_PIN);
   ScotchYork.attach (ScotchYork_PIN);
   attachInterrupt (1,Hit_Detection,RISING);
   for ( int i=0 ; i<10 ; i++) {
     pinMode (PIN_Array[i], OUTPUT);
   }
   
   LW.writeMicroseconds (LWStop);
   RW.writeMicroseconds (RWStop); 
   ScotchYork.write (Cannon_hold);
//  digitalWrite ( Cannon_LM_Black_PIN , LOW) ; 
//   digitalWrite (Cannon_RM_Black_PIN , HIGH) ;
   Cannon_on (); 
   delay (1000);
   Cannon_off ();  
   Sonar.write (angle_mid);  
   for (int i=0 ; i < angle_number ; i++)
   {
     angleArray [i] = ( ( i* angle_increment) + angle_right );
   } 
} 

void Hit_Detection () {
  Hit_Flag = HIGH ;
}

int Life (int Hit_Count) {
  static int Life_Remained = 20 ;
  Life_Remained = Life_Remained - Hit_Count ;
   return Life_Remained ;
}

void Led_Display ( int Life_Remained ) {
  int Led_Value1 ; int Led_Value2;
  int i = Life_Remained % 10 ;
  int j = (Life_Remained / 10 );
  Led_Value1 = Seg_Array [i]; 
  if ( j == 0 ) Led_Value2 = 255 ;
  if ( j > 0 ) Led_Value2 =  Seg_Array [j];
  digitalWrite (HC595_Latch_PIN, LOW);
  shiftOut (HC595_Data_PIN,HC595_Clock_PIN,MSBFIRST, Led_Value1);
  shiftOut (HC595_Data_PIN,HC595_Clock_PIN,MSBFIRST, Led_Value2);
  digitalWrite (HC595_Latch_PIN, HIGH);
}
 
void Cannon_fire ( )
{
  ScotchYork.attach (ScotchYork_PIN);
  ScotchYork.write (Cannon_hold);
  delay (500); 
  Cannon_on ();
  delay (1000);
  ScotchYork.write (Cannon_load); 
  delay (1500);
  ScotchYork.write (Cannon_push);
  delay (1000);
}

void Cannon_on ()
{
  int motor_Val_run = 180 ;                 
  analogWrite(Cannon_LM_Red_PIN, motor_Val_run);
  analogWrite(Cannon_RM_Red_PIN, motor_Val_run);
}

void Cannon_off ()
{
  ScotchYork.write (Cannon_hold); 
  ScotchYork.detach ();        
  analogWrite(Cannon_LM_Red_PIN, 0);
  analogWrite(Cannon_RM_Red_PIN, 0);
}

void Measuredis ( int *distanceArray, int *angleArray) 
{
    int angle_step = angle_increment/5 ;
    int angle_init = angle_mid ;
    for ( int angle = angle_init ; angle >= angleArray [0] ; angle -= angle_step )
    {
      Sonar.write (angle);
      delay (100);
    }
    angle_init = angleArray [0] ;
    for ( int i = 0 ; i < angle_number ; i++ )
    {
      for ( int angle = angle_init ; angle <= angleArray [i] ; angle += angle_step)
      {
        Sonar.write (angle);
        delay (100);
      }
      Sonar.write (angleArray [i]) ;
      delay (100);
      distanceArray [i] = sonar.ping_cm();
      delay (100);
      angle_init = angleArray [i] ;
    }
    for ( int angle = angle_init ; angle >= angle_mid ; angle -= angle_step )
    {
      Sonar.write (angle);
      delay (100);
    }
    Sonar.write (angle_mid);   
}

int Mindis ( int *distanceArray, int *angleArray, int * minAngle) 
{ 
   int minx = MAX_DISTANCE ;
   for (int i = 0; i < angle_number ; i++) 
   {
      if ( distanceArray [i] != 0 ) 
      {   
        if ( distanceArray [i] < minx ) 
        {
           minx = distanceArray [i] ;
           *minAngle = angleArray [i] ;
        }
      }       
   }        
   return minx;
} 

void Car_stop ()
{
  LW.writeMicroseconds (LWStop);
  RW.writeMicroseconds (RWStop);
  delay (200);
}

void Car_backward (int LWB, int RWB )
{
  LW.writeMicroseconds (LWStop - 130 * LWB);
  RW.writeMicroseconds (RWStop + 120 * RWB);
  delay (1500);
}

void Car_forward (int LWF, int RWF )
{
  LW.writeMicroseconds (LWStop + 130 * LWF);
  RW.writeMicroseconds (RWStop - 120 * RWF);
  delay (1500);
} 

void loop () 
{ 
  Life_Remained = Life (Hit_Count);
  Led_Display (Life_Remained) ;  
  
  if (Hit_Flag == HIGH ) {
    if ( Life_Remained > 0 ) {
      Hit_Count = 1 ;
    }
    else {
      Hit_Count = -20 ;
    }
    Life_Remained = Life (Hit_Count);   
    Led_Display (Life_Remained);
    delay (200);
    Hit_Count = 0;
    Hit_Flag = LOW ;
  }
  
 // int minx = 0 ;
 // int minAngle = 90 ;
//  int target_direction = 0 ;
  Car_stop ();
  //Cannon_off (); 
  Cannon_fire ();
  Cannon_off ();
  delay (1000); 
 /*
  Sonar.attach (Sonar_PIN);
  Measuredis ( distanceArray , angleArray );
  minx = Mindis ( distanceArray, angleArray, &minAngle );
  Sonar.detach ();
  delay (200);
    Serial.print ("   min distance="); Serial.print (minx);
    Serial.print ("   min angle="); Serial.println (minAngle);
    delay (1000);
    
  if (minAngle == 90 ) target_direction = 0 ;
  if (minAngle < 90 ) target_direction = 1 ;
  if (minAngle > 90 ) target_direction = 2 ;
  
  if ( minx == MAX_DISTANCE ) {  
   Car_forward (1,1);
  }
  else {  
    if ( minx <= 15 ) {
       switch (target_direction) {
         case 0 :
             Car_backward (1,1);
           break;
         case 1 :
              Car_backward (0,1);
           break;
         case 2 :
              Car_backward (1,0);
           break;
       }
     }     
   else {
      switch (target_direction) {
         case 0 :
             Cannon_fire ();
             Cannon_off ();
           break;
         case 1 :
             Car_forward (0,1);
           break;
         case 2 :
             Car_forward (1,0);
           break;
      }             
    }
  } 
  */
}

